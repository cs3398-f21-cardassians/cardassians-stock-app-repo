import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`

  * {
    color: ${({ theme }) => theme.text};
    input {
      color: black !important;
      border-radius: 5px;
    }
    button {
      border-radius: 10px;
      padding: 5px;
      background-color: ${({ theme }) => theme.buttonColor} !important;
    }
  }

  .app {
    background-color: ${({ theme }) => theme.backgroundColor} !important;
  }

  .log-in-box {
    background-color: ${({ theme }) => theme.accent} !important;
  }

  .holder {
    background-color: ${({ theme }) => theme.accent} !important;
  }

  .modal-main {
    background-color: ${({ theme }) => theme.backgroundColor} !important;
  }

  .stock-card {
    background-color: ${({ theme }) => theme.stockCard} !important;
  }

`;
