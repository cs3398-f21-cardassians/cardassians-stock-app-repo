import { timeParse } from "d3-time-format";

function parseData(data, isSmall) {
  let arr = [];
  const name = isSmall ? "Time Series (5min)" : "Time Series (Daily)";
  const onlyNeededInfo = data[name];
  let multiplier = 1;
  if (onlyNeededInfo) {
    Object.entries(onlyNeededInfo).forEach(([key, value]) => {
      var line = {
        date: isSmall ? parseDateAndTime(key) : parseDate(key),
        open: value["1. open"] / multiplier,
        high: value["2. high"] / multiplier,
        low: value["3. low"] / multiplier,
        close: value["4. close"] / multiplier,
        volume: value["6. volume"] / multiplier,
      };
      if (!isSmall) multiplier = multiplier * value["8. split coefficient"];
      arr.unshift(line);
    });
  }
  return arr;
}

const parseDate = timeParse("%Y-%m-%d");
const parseDateAndTime = timeParse("%Y-%m-%d %H:%M:%S");

export async function getData(symbol) {
  let promiseMSFT = await fetch(
    `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=${symbol}&apikey=DCYUOA2K7RCAQBFE&outputsize=full`
  );
  promiseMSFT = await promiseMSFT.json();
  promiseMSFT = parseData(promiseMSFT, false);
  return promiseMSFT;
}

export async function getDataSmall(symbol) {
  let promiseMSFT = await fetch(
    `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${symbol}&interval=5min&apikey=DCYUOA2K7RCAQBFE&outputsize=full`
  );
  promiseMSFT = await promiseMSFT.json();
  promiseMSFT = parseData(promiseMSFT, true);
  return promiseMSFT;
}

export async function getDataFull(symbol) {
  let fullArray = [];
  while (fullArray.length < 2) {
    const bigArr = await getData(symbol);
    const smallArr = await getDataSmall(symbol);
    fullArray = [...bigArr, ...smallArr];
    fullArray.sort((a, b) => {
      return a.date - b.date;
    });
  }
  return fullArray;
}
