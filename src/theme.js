export const lightTheme = {
  text: "black",
  backgroundColor: "#fffdf5",
  accent: "rgb(58, 69, 219)",
  buttonColor: "ivory",
  stockCard: 'slategray',
};

export const darkTheme = {
  text: "white",
  backgroundColor: "grey",
  accent: "black",
  buttonColor: "#212529",
  stockCard: 'black'
};
