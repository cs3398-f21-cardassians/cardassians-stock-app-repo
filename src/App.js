import React, { useState } from "react";
import StockCard from "./StockCard";
import Login from "./LogIn";
import Newsfeed from "./Newsfeed";
import ThemeSelector from "./ThemeSelector";
import "./CSS/App.css";
import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from "./theme";
import { GlobalStyles } from "./global";

const App = () => {
  const savedStock = localStorage.getItem("lastStock");
  const firstStock =
    savedStock === null || localStorage.getItem("cleanStock") !== "true"
      ? "AAPL"
      : savedStock;
  const [name, setName] = useState(firstStock);
  const [price, setPrice] = useState(0);
  const [floor, setFloor] = useState(0);
  const [theme, setTheme] = useState(0);

  const nameChangeHandler = (event) => {
    if (event.key === "Enter") setName(event.target.value);
  };

  const priceChangeHandler = (event) => {
    if (event.key === "Enter") setPrice(+event.target.value);
  };

  const floorChangeHandler = (event) => {
    if (event.key === "Enter" && !isNaN(+event.target.value)) {
      setFloor(+event.target.value);
    }
  };

  const toggleTheme = () => {
    setTheme((prev) => prev + 1);
  };

  return (
    <ThemeProvider theme={theme % 2 === 0 ? lightTheme : darkTheme}>
      <GlobalStyles />
      <div className="app">
        <div className="chart-editors">
          <div className="box-and-label">
            Stock Abbreviation:
            <input type="text" onKeyDown={nameChangeHandler} />
          </div>
          <div className="box-and-label">
            Price Bought At:
            <input type="number" onKeyDown={priceChangeHandler} />
          </div>
          <div className="box-and-label">
            Graph Lower Bound:
            <input type="number" onKeyDown={floorChangeHandler} />
          </div>
        </div>
        <StockCard stockName={name} floor={floor} priceBought={price} />
        <div className="log-in-box">
          <Login />
        </div>
        <Newsfeed />
        <ThemeSelector theme={"light"} toggleTheme={toggleTheme} />
        <div style={{ fontSize: "10px" }}>{theme}</div>
      </div>
    </ThemeProvider>
  );
};

export default App;
