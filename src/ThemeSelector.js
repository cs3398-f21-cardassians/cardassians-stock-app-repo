import React from "react";
import MoonIcon from "./icons/moon.svg";
import SunIcon from "./icons/sun.svg";
import "./CSS/ThemeSelector.css";

const ThemeSelector = (props) => {
  return (
    <button onClick={props.toggleTheme} className="theme-selector">
      <img src={SunIcon} alt="sun icon" className="lil-icon" />
      <img src={MoonIcon} alt="moon icon" className="lil-icon" />
    </button>
  );
};

export default ThemeSelector;
