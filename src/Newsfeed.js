import "./CSS/Newsfeed.css";
import React, { useEffect, useState } from "react";

const Newsfeed = (props) => {
  const articles = [
    "Stocks and Oil Drop Amid New Coronavirus Variant",
    "CANADA STOCKS-TSX drops as energy, material stocks fall",
    "Tech and healthcare firms drag Hong Kong shares lower",
    "These are the 10 most talked about stocks on Reddit's WallStreetBets",
    "Tech stocks will keep feeling pressure",
  ];
  const urls = [
    "https://www.nytimes.com/2021/11/26/business/covid-variant-stock-market-oil-prices.html",
    "https://www.reuters.com/article/canada-stocks-idUSL4N2S63X4",
    "https://www.reuters.com/article/china-stocks-hongkong-close-idUSAZN01SFCZ",
    "https://markets.businessinsider.com/news/stocks/tesla-rivian-stock-most-mentioned-reddit-wallstreetbets-meme-retail-2021-11",
    "https://www.reuters.com/video/watch/idOVF4TQPHN",
  ];

  const [newsItems, setNewsItems] = useState([]);

  useEffect(() => {
    fetch(
      "https://newsapi.org/v2/everything?q=stocks&apiKey=134817875ad14e7181fdcd2fd6229c51"
    )
      .then((response) => response.json())
      .then((articles) => {
        setNewsItems([...newsItems, ...articles]);
      })
      .catch((error) => console.log(error));
  });

  /*const NewsItem = (article, id) => (
    <li key={id}>
      <a href={`${article.url}`}>{article.title}</a>
    </li>
  );*/

  //const newsItemsComp = newsItems.map((item, index) => NewsItem(item, index));

  const [counter, setCounter] = useState(0);

  const detectPress1 = () => {
    counter >= 1
      ? setCounter((prev) => prev - 1)
      : setCounter(articles.length - 1);
  };

  const detectPress2 = () => {
    counter <= articles.length - 2
      ? setCounter((prev) => prev + 1)
      : setCounter(0);
  };

  return (
    <div className="holder">
      <div className="article-link">
        <a href={urls[counter]} target={"_blank"}>
          <h1 style={{ fontSize: 25, color: "white" }}>{articles[counter]}</h1>
        </a>
      </div>

      <div className="bottombar">
        <div className="buttons">
          <button className="side-button" onClick={detectPress1}>
            {"<"}
          </button>
          <button className="side-button" onClick={detectPress2}>
            {">"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Newsfeed;
