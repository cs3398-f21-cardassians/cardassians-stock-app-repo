// Addition of modal component. Allows user to set tracking settings.
import React, { useState } from "react";
import Modal from "./Modal.js";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ModalDash = (props) => {
  const { stockName, price } = props;
  console.log("price inside modal dash: " + price);

  const [upperBoundNotify, setUpper] = useState(0);
  const [lowerBoundNotify, setLower] = useState(0);

  // Setting up and lower bound numbers to track.
  const upperBoundNumber = UpperThreshold(price, upperBoundNotify);
  const lowerBoundNumber = LowerThreshold(price, lowerBoundNotify);

  const upperBoundChangeHandler = (event) => {
    if (event.key !== "Enter") return;
    setUpper(+event.target.value);
    const upperBoundNumber = UpperThreshold(price, +event.target.value);
    console.log("upper bound threshold = " + upperBoundNumber);
  };

  const lowerBoundChangeHandler = (event) => {
    if (event.key !== "Enter") return;
    setLower(+event.target.value);
    const lowerBoundNumber = LowerThreshold(price, +event.target.value);
    console.log("lower bound threshold = " + lowerBoundNumber);
  };

  async function fetchCurrentPrice(
    stockName,
    upperBoundNumber,
    lowerBoundNumber
  ) {
    const response = await fetch(
      `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${stockName}&apikey=DCYUOA2K7RCAQBFE`
    );
    const body = await response.json();
    const CurrentData = body["Global Quote"];
    const currentPrice = CurrentData["05. price"];
    console.log("currentprice inside fetch = " + currentPrice);

    // If threshold number reached, fire appropriate notification.
    if (currentPrice > upperBoundNumber) {
      console.log("i am here, upperbound is: " + upperBoundNumber);
      toast.success(
        "Sell threshhold number reached. Consider selling: " + stockName,
        {
          autoClose: false,
        }
      );
    } else if (currentPrice < lowerBoundNumber) {
      toast.warn("Stock " + stockName + " dropped beneath threshold number.", {
        autoClose: false,
      });
    }
  }

  const [show, setShow] = useState(false);

  const showModal = () => {
    setShow(true);
  };

  const hideModalAndTrack = () => {
    setShow(false);
    console.log("I clicked start tracking");
    toast.configure();
    toast("Now tracking: " + stockName);

    // Tracking established.
    setInterval(
      fetchCurrentPrice(stockName, upperBoundNumber, lowerBoundNumber),
      360000
    );
  };

  const hideModal = () => {
    setShow(false);
  };

  return (
    <React.Fragment>
      <Modal show={show} handleClose={hideModal}>
        <section className="modal-main">
          <h5>Set tracking settings</h5>
          Notify when stock goes up by this percentage.
          <br />
          (1-100, press Enter to confirm):
          <br />
          <input type="number" onKeyDown={upperBoundChangeHandler} />
          <br />
          <br />
          Notify when stock drops by this percentage.
          <br />
          (1-100, press Enter toconfirm):
          <br />
          <input type="number" onKeyDown={lowerBoundChangeHandler} />
          <br />
          <br />
          <button onClick={hideModal}>Close</button>
          <button onClick={hideModalAndTrack}>Start tracking</button>
        </section>
      </Modal>
      <button onClick={showModal}>Track stock</button>
    </React.Fragment>
  );
};

export default ModalDash;

const UpperThreshold = (price, upperBoundNotify) => {
  var userDesiredUpperThreshold = upperBoundNotify / 100;

  const upperNotify = price + price * userDesiredUpperThreshold;
  return upperNotify;
};

const LowerThreshold = (price, lowerBoundNotify) => {
  var userDesiredThreshold = lowerBoundNotify / 100;

  const lowerNotify = price - price * userDesiredThreshold;
  return lowerNotify;
};
