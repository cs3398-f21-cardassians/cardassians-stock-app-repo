import React from "react";
import PropTypes from "prop-types";

import { scaleTime } from "d3-scale";
import { curveMonotoneX } from "d3-shape";
import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import { AreaSeries } from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import { fitDimensions } from "react-stockcharts/lib/helper";
import {
  createVerticalLinearGradient,
  hexToRGBA,
} from "react-stockcharts/lib/utils";
import { PriceCoordinate } from "react-stockcharts/lib/coordinates";
import {
  MouseCoordinateX,
  MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";

const canvasGradient = createVerticalLinearGradient([
  { stop: 0, color: hexToRGBA("#95CF77", 0.2) },
  { stop: 0.7, color: hexToRGBA("#63C72D", 0.4) },
  { stop: 1, color: hexToRGBA("#4CDB00", 0.8) },
]);

const negativeGradient = createVerticalLinearGradient([
  { stop: 0, color: hexToRGBA("#EA0000", 0.6) },
  { stop: 0.4, color: hexToRGBA("#FF0F0F", 0.4) },
  { stop: 1, color: hexToRGBA("#FC5E5E", 0.2) },
]);

const getLine = (bought) => {
  if (bought)
    return (
      <PriceCoordinate
        at="right"
        orient="right"
        price={bought}
        displayFormat={format(".2f")}
        lineOpacity={0.8}
        lineStroke="#FFFFFF"
        strokeDasharray="ShortDash"
      />
    );
};

const getRed = (bought) => {
  if (bought)
    return (
      <AreaSeries
        yAccessor={(d) => Math.min(d.close, bought)}
        fill="url(#MyGradient)"
        strokeWidth={2}
        stroke="firebrick"
        interpolation={curveMonotoneX}
        canvasGradient={negativeGradient}
        type="hybrid"
      />
    );
};

class AreaChart extends React.Component {
  render() {
    const { data, width, height, ratio, stockName, yMin, xMin, boughtAt } =
      this.props;

    return (
      <ChartCanvas
        ratio={ratio}
        width={width}
        height={height}
        margin={{ left: 50, right: 50, top: 10, bottom: 30 }}
        seriesName={stockName}
        data={data}
        type="hybrid"
        xAccessor={(d) => {
          if (d === undefined) return d;
          return d.date;
        }}
        xScale={scaleTime()}
        xExtents={[xMin, new Date()]}
        pointsPerPxThreshold={10}
        clamp={true}
      >
        <Chart
          id={0}
          yExtents={(d) => [Math.max(d.high, d.low), Math.min(d.low, yMin)]}
        >
          <defs>
            <linearGradient id="MyGradient" x1="0" y1="100%" x2="0" y2="0%">
              <stop offset="0%" stopColor="#b5d0ff" stopOpacity={0.2} />
              <stop offset="70%" stopColor="#6fa4fc" stopOpacity={0.4} />
              <stop offset="100%" stopColor="#4286f4" stopOpacity={0.8} />
            </linearGradient>
          </defs>
          <XAxis
            axisAt="bottom"
            orient="bottom"
            ticks={6}
            tickStroke="#FFFFFF"
            stroke="#FFFFFF"
          />
          <YAxis
            axisAt="left"
            orient="left"
            tickStroke="#FFFFFF"
            stroke="#FFFFFF"
          />
          <AreaSeries
            yAccessor={(d) => d.close}
            fill="url(#MyGradient)"
            strokeWidth={2}
            stroke="green"
            interpolation={curveMonotoneX}
            canvasGradient={canvasGradient}
            type="hybrid"
          />
          {getRed(boughtAt)}
          {getLine(boughtAt)}
          <MouseCoordinateX
            at="bottom"
            orient="bottom"
            displayFormat={timeFormat("%Y-%m-%d")}
          />
          <MouseCoordinateY
            at="left"
            orient="left"
            displayFormat={format(".4s")}
          />
        </Chart>
      </ChartCanvas>
    );
  }
}

AreaChart.propTypes = {
  data: PropTypes.array.isRequired,
  width: PropTypes.number.isRequired,
  ratio: PropTypes.number.isRequired,
  type: PropTypes.oneOf(["svg", "hybrid"]).isRequired,
};

AreaChart.defaultProps = {
  type: "svg",
};
AreaChart = fitDimensions(AreaChart);

export default AreaChart;
