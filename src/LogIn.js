import * as Axios from "axios";
import React, { useState } from "react";
import "./CSS/LogIn.css";

function LogIn() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(null);
  const [registerStatus, setRegisterStatus] = useState(null);

  function validateFormFields() {
    return (
      username.length > 0 &&
      password.length > 0 &&
      username.length < 20 &&
      password.length < 20
    );
  }

  function handleSubmit(event) {
    event.preventDefault();
    console.log(username);
    console.log(password);
    localStorage.setItem("username", username);
    setIsLoggedIn(true);
  }

  function handleRegister(event) {
    event.preventDefault();

    Axios.get("/verify", {
      username: username,
      password: password,
    })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });

    /*
    Axios.post("/register", {
      username: username,
      password: password,
    }
    ).then((response) => {
      localStorage.
    })
    */

    setIsLoggedIn(true);
    setRegisterStatus(false);
  }

  function logOut() {
    localStorage.removeItem("username");
    setIsLoggedIn(false);
  }

  function flipRegisterStatus(event) {
    event.preventDefault();
    if (registerStatus === false || registerStatus === null) {
      setRegisterStatus(true);
      localStorage.setItem("username", username);
    } else {
      setRegisterStatus(false);
    }
  }

  if (registerStatus === true) {
    return (
      <div className="register">
        <h3>Register</h3>
        <br />
        <form onSubmit={handleRegister}>
          <label>
            <input
              type="text"
              placeholder="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          <br />
          <label>
            <input
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <button type="submit" disabled={!validateFormFields()}>
            Register
          </button>
          <button onClick={flipRegisterStatus}>Back</button>
        </form>
      </div>
    );
  } else if (isLoggedIn === false) {
    return (
      <div className="log-in">
        <h3>Log in</h3>
        <br />
        <form onSubmit={handleSubmit}>
          <label>
            <input
              type="text"
              placeholder="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          <br />
          <label>
            <input
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <div>
            <button type="submit" disabled={!validateFormFields()}>
              Submit
            </button>
            <button onClick={flipRegisterStatus}>Register</button>
          </div>
        </form>
      </div>
    );
  } else {
    return (
      <div className="logged-in">
        <div className="log-out-box">
          <h3>Welcome back {localStorage.getItem("username")}.</h3>
          <br />
          <button onClick={logOut}>Log out</button>
        </div>
      </div>
    );
  }
}

export default LogIn;
