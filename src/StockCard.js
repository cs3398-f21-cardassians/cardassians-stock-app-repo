import React, { useEffect, useState } from "react";
import Chart from "./Chart";
import { getDataFull } from "./utils";
import "./CSS/StockCard.css";
import ModalDash from "./ModalDash.js";

const StockCard = ({ stockName, floor, priceBought }) => {
  const [data, setData] = useState(null);
  const [myName, setMyName] = useState(null);

  const getAndSetData = () => {
    getDataFull(stockName).then((data) => {
      if (data.length > 1) {
        setData(data);
        setMyName(stockName.toUpperCase());
        localStorage.setItem("lastStock", stockName);
        localStorage.setItem("cleanStock", "true");
      }
    });
  };

  useEffect(() => {
    getAndSetData();
    const timer = setInterval(() => {
      getAndSetData();
    }, 60000);
    return () => clearInterval(timer);
  }, [stockName]);

  if (data == null) {
    return (
      <div className="stock-card">
        <div className="chart-container">
          <div style={{color: 'white'}}>Loading...</div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="stock-card">
        <h3>{myName}</h3>
        <div className="chart-container">
          <Chart
            data={data}
            symbol={myName}
            yMin={floor}
            xMin={new Date(2021, 0, 0)}
            boughtAt={priceBought}
          />
        </div>
        <ModalDash price={priceBought} stockName={myName} />
      </div>
    );
  }
};

export default StockCard;
