make sure you have npm or yarn installed

if you have yarn, enter the following command into command line (within this directory) :
yarn install && yarn start

if you have npm, enter the following command into command line (within this directory) :
npm install && npm start

after that, the app should open automatically in your browser, if not goto "localhost:3000" in a browser

We do have a development branch but the branch that was demoed in class is main.  

Project Status: Completed Sprint 3  - www.cardassiansstocks.com  
  
Our webapp is coming together, for sprint 3 we were able to refactor a lot of code, host application online, build with CircleCI, enhance the UI by adding a theme selector, and added a modal component for the user to select the exact percentage they want to be notified of when the stock reaches it.

Features: **FORMAT: <what it is used for, when it is used, why you did it etc. what this artifact does or what you did with it>  < link to bitbucket>**

>Armando Castillo:  My additions can be found in the StockCard.js file. They consist of the creation of a button using a library called toast. I imported the library and added the necessary code for the button and function to notify when the button is pressed. For next sprint I intend to connect the button to another component that will enable the actual tracking of the stock and notify the user at a certain threshold.https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/armando/src/StockCard.js  

>Skylar Crittenden: I built the backend to what will become the newsfeed in the finalized app, which will be used in tandem with the notify function to keep users.
                    up to date with news regarding their owned stocks. It shows an updated (every 5 minutes) list of news articles regarding stocks, which will 
                    be filtered later on to only show those the user is interested in. It was not in a appealing visual format, so the next step is to try to
                    change that, as well as implement it into the existing app. 
                    https://bitbucket.org/cs3398-f21-cardassians/skylars-repository/src/master/newsfeed/news-app/  

>Clayton Strike:    I built logIn.js, along with its CSS file and a very small bit of code on the App.js and App.css files. Effectively I built the 
                    side bar component on the frontend and basically the frontend skeleton of what will be our log in implementation. Currently the 
                    log in componenet just saves the user's information into local storage. Over the course of the next sprint, the plan is to get
                    the log in component fully functional and maybe display the user's saved stock choices as well. My main code, logIn.js, can be 
                    found here: https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/LogIn.js. I implemented the log 
                    in integration because we knew that to save user stock information, we were going to need personalized user files. The component 
                    was integrated as a sidebar menu because we intend to display the user's portfolio in that sidebar post log-in. The rest of 
                    my code can be found on my bitbucket branch clayton. My files were pushed onto the main and demo branch by teammates this sprint 
                    because somehow I've royally messed up my git integration on my local machine - I'm planning on meeting with a TA over the course 
                    of this or next week to get that fixed. 

>Kevin Wentland:    I built the actual "card" that displays the stock graph. The files that created while doing this can be found in the src folder
                    the files are "utils.js", "StockCard.js" and "Chart.js" I will include a link to to the branch where I worked on all of this
                    I also quickly made the input components in App.js just for the purposes of demo-ing our app and to show the capabilities of the
                    StockCard that I created. I did all of the styling for my components, the styling is in CSS files matching the component names.
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/kevin/src/
                    
Features: (Sprint 2) 

>Kevin Wentland:    I made a lot of improvements to the StockCard component and the utils.js file. I updated the parseData function in the utils file
                    to be able to parse data from both the daily time interval api, and the 5 minute interval api. I then made a function to combine these data
                    points from the api, and then sort them by date. I also improved a couple of bugs that caused the app to crash. For example, making too many
                    API calls to our free api will cause the api to return an error message, this was causing the app to crash. Also, I disallowed users from viewing empty data 
                    on the graph. I also setup a pipeline to automatically deploy the main branch to our website. My next steps will be integrating user metadata into the StockCard.
                    I also improved some styling throughout the app, but most of my contributions can be found in the following two files:
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/utils.js
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/StockCard.js  

>Armando Castillo: For sprint 2 my additions can be found in the TrackStockButton.js file. I've moved the code and functionality for the initial button to this file and also added the ability to track a stock and notify a user when a certain price threshold has been reached. There are three notifications that come with the button. The initial notification that states a stock is now being tracked, another that alerts when the price has gone up past the threshold, and lastly one that alerts when the price has dropped below another separate threshold. The branch I was working on was deleted when it was merged to our main, all history can be found in the commit section of our program. File is found here https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/TrackStockButton.js . My next goals are to allow the user to set their own notification settings, and ideally connect it to a user account that has been created.

>Skylar Crittenden: I built the frontend of the newsfeed component. It allows for toggling between selected news articles gathered on the criteria of 
                    stocks the user is interested in. In it's current state, it holds up to 5 different URLs, which at the time, are hard coded, but this
                    will be changed in the next sprint, ideally. It serves the purpose of better informing the user as to up and coming news from the stock
                    market, which will allow them to make better decisions insofar as their investments.
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/Newsfeed.js

>Clayton Strike: I wrote the backend code for the login component, it now registers and verifies users using a local MySQL server. I also added some frontend code to display the register functionality. I also bought us a domain name and registered a remote MySQL database, but I have a bug where the port the code is supposed to connect to is busy. Next steps for sprint 3 will be completing the log in implementation completely by having a functional connection to a remote database, or switching remote databases if I can't figure out the current bug. https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/commits/b1e8bcf5f483d921636106c2896ce142f81db396?at=clayton

Features: (Sprint 3)

>Skylar Crittenden: I connected the newsfeed to an api, with the intention to pull recent news articles regarding stocks into the newsfeed located to the right of
                    the application, so that the user might be better informed as to the current state of the market. Unfortunately, changes down the line 
                    put the api calls into a state of disrepair, imparing a few of the newer features implemented, and we were unable to remedy it entirely before the
                    demo. If given a few more days, this feature could have been restored to it's full state. Code located here: 
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/Newsfeed.js

>Kevin Wentland:    I completely built the ThemeSelector during this sprint and I refactored the StockCard component so that it is now a functional component instead of being
                    class based. The ThemeSelector was built completely from scratch, and it is currently able to switch between light and dark mode. The logic is all set up so
                    more themes can easily be created and used, and the logic is ready to have users create custom themes themselves. StockCard and ThemeSelector can be found here:
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/StockCard.js
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/ThemeSelector.js
                    The next step for me would be expand on the current ThemeSelector by creating more preset themes and also allow users to create their own themes.
                    Or, another good next step would be to get the backend working so we can track user info across browsing devices.
                    
>Clayton Strike:    I refactored my old backend code for serverIndex.js including the SQL queries, finished up the backend code there. 
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/serverIndex.js
                    I can't remember if I added more code this sprint, but I was also responsible for the login component frontend and might've refactored a little bit. 
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/LogIn.js
                    https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/CSS/LogIn.css
                    Then I tested all the code with a local database and made sure my backend code worked correctly. 
                    Then I set up a remote database using heroku, and after a lot of debugging got our server to connect to that remote database. After more debugging, I got the ports between
                    the server and database to connect. Finally, I was getting a bug that caused a 404 error whenever I tried to run my SQL queries using the node server connected to 
                    heroku remote SQL database. I wasn't able to figure out that bug by the end of the sprint. Ultimately, most of this sprint ended up being debugging and messing with 
                    dependencies. I really had to spend just an insane amount of time on it and I would've benefitted a lot from having access to a senior engineer mentor - probably spent 
                    10+ hours just on the final bug I couldn't solve. The next step for me would be to solve that final bug. As far as I'm aware, once that issue is solved the login component 
                    would be fully functional. After that, I'd like to add an additional query to that database where we save the user's stock choices and purchase prices. 
                    
>Armando Castillo:   For sprint 3 I refactored my code and then converted what was previously the TrackStockButton.js file into a modal component. When clicked the button then prompts the user to enter exactly the percent change of when they initially purchased the stock to notify them of it. The creation of the modal component also included the addition of two new files that are found here. https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/Modal.js The main bulk of the modal component code is found here: https://bitbucket.org/cs3398-f21-cardassians/cardassians-stock-app-repo/src/main/src/ModalDash.js . Ideally, my next step would be to link the tracking feature to actual user accounts so if the project were to continue I would focus all of my attention to helping in getting the database and user profile aspect of the website up and running so users can log in and check on their stocks.