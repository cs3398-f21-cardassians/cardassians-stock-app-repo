// gotta npm install express, mysql, axios, cors

const express = require('express');
const app = express(); 
const mysql = require('mysql'); 
const axios = require('axios'); 
const cors = require("cors");

app.use(cors()); 
app.use(express.urlencoded({ extended: true } )); 
app.use(express.json()); 


// this code is very important, don't change
// we need this information to log in to database
const db = mysql.createConnection({
    user: "bc21afa7a4e6f5",
    host: "us-cdbr-east-04.cleardb.com", 
    password: "35ffbc32",
    database: "heroku_1d49a90e9b60e0d",
    
    timeout: 600000,
});


db.connect(function(err) {
    if (err) {
        throw err;
    }
});



app.listen(3001, (req, res) => {
    console.log("Yay, the server is running."); 
});


app.post("/verify", (req, res) => {
    
    const username = req.body.username; 
    const password = req.body.password; 

    db.query(
        "SELECT count(*) AS flag FROM users WHERE username=(?) AND password=(?)",
        [username, password],
        (err, result) => {
            if (err) {
                console.log("Error when checking username/password validity."); 
                console.log(err); 
            }
            else {
                res.send("Register success!"); 
            }
        }
    )
})

app.post("/register", (req, res) => {

    const username = req.body.username; 
    const password = req.body.password; 

    console.log("made it into backend register"); 

    db.query(
        "INSERT INTO users (username, password) VALUES (?, ?)", 
        [username, password], 
        (err, result) => {
            if (err) {
                console.log("Error during register."); 
                console.log(err); 
            }
            else {
                res.send("Register success!"); 
            }
        }
    );
}); 

app.post("/logIn", (req, res) => {
    
    const username = req.body.username; 
    const password = req.body.password; 

    db.query(
        "SELECT count(*) AS flag FROM users WHERE username=(?) AND password=(?)",
        [username, password], 
        (err, result) => {
            if (err) {
                console.log("Error when checking login validity."); 
                console.log(err); 
            }
            else {
                console.log("made it to else in server"); 
                res.json( { "flag": result[0].flag } ); 
            }
        }

    );
});